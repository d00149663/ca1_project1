﻿//Author: Masood Ahmed (D00149663)
//Project: CA1
//Module: Games Programming
//Youtube: http://www.youtube.com/watch?v=3U4qZmtI-1E&feature=youtu.be&hd=1
//this is a text based game while is simple text based giving useroptions to how to navigate through game
// The game will have colours and sound effects on some parts
#include <iostream>// standard input/output library for console.
#include <string>// is to handle strings
#include <windows.h>//used to maximize command screen
#include <cstdlib> // To be able to use system it is needed as an identifier

using namespace std;

//Methods included
void Start();
void Game();
void gameStart();
void Intro();
void chooseClass();
void chapter1();
void quest();
void gameOver();

//using 2 inputs to understand what happens in what input
int userInput = 0;
int playerLocation = 0; // A numerical reference to the location of the player

int main()//main function
{	
	//tells screen to maximize
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_MAXIMIZE);

	Start();
	//commented code below ws to test each methods out seperatly before linking them in the methods
	//Game();
	//Intro();
	//chooseClass();
	//chapter1();
	//quest();
	//gameOver();
	system("exit");
}
void Start() //intro and few title lines to game
{
	system("cls");
	system("color c");//change the text colour
	cout << "********************************************************************************\n";
	cout << "******************             UNTOLD STORY OF HELL        *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300); // beep is the sound beeping first number is the tone hi or low and second number is how long the tone goes
	Beep(150, 200);
	Beep(250, 200);

	system("cls"); // it clears the text in the system
	cout << "********************************************************************************\n";
	cout << "******************        A New Story To be Discovered!     ********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************                 One Dream	            *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************               Real or fake              *********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("cls");
	cout << "********************************************************************************\n";
	cout << "******************           An Un-escapeable Dream         ********************\n";
	cout << "********************************************************************************\n";
	Beep(80, 300);
	Beep(150, 200);
	Beep(250, 200);
	system("pause");
	system("cls");
	Game();//method that goes into game
}
void Game()//main menu in game
{
	
	system("color e");
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "****************** [1]            New Game                 *********************\n";
	cout << "****************** [2]            Credits                  *********************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	Beep(550, 500);	

	
	cout << "\n\n>\t";	
	
	while (!(cin >> userInput) || userInput < 1 || userInput > 3)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	}

	if (userInput == 1)
	{
		// Create a new game
		system("cls");
		gameStart();//method going into gamestart
	}
	else
	{
		// Game over
		system("cls");
		gameOver();
	}


}
void gameStart()//first chapter headings
{
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "******************             Chapter ONE                 *********************\n";
	cout << "******************           The Eternal Dream to Hell     *********************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	Beep(350,100);
	Beep(200,300);
	Beep(300,200);
	Beep(200,300);
	Beep(300,200);

	system("pause");
	system("cls");
	Intro();//method going into into method
		
}
void Intro()//user input name
{
	system("color 3");
	cout << "Aloha Adventurer what shell be your Last Name\n";
	string name;
	cout << "\n>\t";
	cin >> name;
	
	cout << "You are " << name << " that is a Strange name \n\n";
	system("pause");
	system("cls");
	chooseClass();
}
void chooseClass()//user input for class they want to be
{	
	int classChoice = 0;
	cout << "What is your playstyle?\n\n";
	cout << "[1] Warrior - balanced/higher damage\n";
	cout << "[2] Wizard - Magic Specilist\n";
	cout << "[3] Mage - support\n";
	
	cout << "\n>\t";
	while (!(cin >> classChoice) || classChoice < 1 || classChoice > 3)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	}
	switch (classChoice)
	{
	case 1:
		cout << "THE WARRIOR IS BORN NOT MADE, HE KNOWS THAT HIS SOUL PURPOSE IN LIFE IS TO SERVE AND FIGHT.\n";
		break;
	case 2:
		cout << "Wizzard. beware of this magic as it could be dangerious if not trained properly\n";
		break;
	case 3:
		cout << "A Mage specialise inn magic of healin and curing\n";
		break;
	default:
		cout << "You are doing it wrong! Press either '1' or '2', or '3' nothing else!\n";
	}			
	system("pause");
	system("cls");
	chapter1();//chapter 1 method
}
void chapter1()//first chapter when game starts with user inout numbers as directions
{
	cout << "You are Awake from your dream in hospital bed in a big city of Nablaska. ";
	cout << "As you walk out the door there are two routes you encounter\n";
	cout << "***************************************************************************************\n";
	cout << "[1] Go East\n";
	cout << "[2] Go West (Recomended not to go this way) voice shouted by someone from the hospital \n";
	cout << "[3] You feeling sick you decide to go back to hospital\n";
	cout << "***************************************************************************************\n";
	cout << "\n>\t";

	int playerLocation = 0;
	while (!(cin >> playerLocation) || playerLocation < 1 || playerLocation > 3)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	}
	if (playerLocation == 1)//East
	{
		system("cls");
		cout << "You go East and find yourself looking into long distance and see a car parked.\n";
		cout << "The car Parked has keys hanging around and you are not in the mood to walk.\n";
		cout << "********************************************************************************\n";
		cout << "[1] pick up keys.\n";
		cout << "[2] ignore the scene and keep heading East.\n";
		cout << "********************************************************************************\n";
		cout << "\n>\t";

		while (!(cin >> userInput) || userInput < 1 || userInput > 3)
		{
			cout << "invalid variable. must be in range of asked choice.\n";
			cin.clear();
			cin.ignore(100, '\n');
		}
		userInput;
		if (userInput == 1)
		{			
			cout << "You pick up keys.\n";
			cout << "You drive towards the end of the road where  you fight a monster and uncover the truth and find out you are in a dream and if you wake up you may die in reality \n if you finish your quest you reach the end goal \n you will live and will be returned back to your real world.\n";
			system("pause");
			system("cls");
			cout << "YOUR QUEST BEGINS HERE\n\n";
			quest();
		}
		else	
		{
			cout << "1: You keep walking and exaust yourself and end up back to hospital\n";
			chapter1();
		}
	}
	else if (playerLocation == 2)//West
	{	
		system("cls");		
		cout << "You go into the city and buy yourself some food \nas you are hungry you buy fast food and eat 10 full meals \nyou pretend nothing is happening\nyou think it is reality and live your life like a normal person\nyou don't realise anything wrong with the picture\nYou are trapped in the dream world\nwith no understanding if it is real or not\neverything seems real\n";
		cout << "\nYou have spent your life not knowing the truth and after awhile you have died off in your old age \nAt the age of 85 \nYou were 15 years old when you arrived in this dream.\n\n";
		system("pause");
		system("cls");
		Beep(700, 1000);
		Beep(500, 2000);
		cout << "YOU HAVE FAILD TO FULFILL YOU DESTINY AND LET THE REAL WORLD TO CHAOS AS IF WAS CONNECTED TO DREAM\nWITH YOU GONE THE CREATURES FOUND THEIR WAY TO THE REAL WORLD\n";
		system("pause");
		system("cls");
		gameOver();
	}
	else if (playerLocation == 3)//Back Hospiatl
	{
		system("cls");
		cout << "You go back in the hospital and take your time thinking and going over your memories but are unable to picture why you ended up in the hospital bed.\n\n";
		Beep(400, 500); // beep is the sound beeping first number is the tone hi or low and second number is how long the tone goes
		Beep(600, 400);
		Beep(400, 400);
		cout << "You have chosen to go back to think about future steps\n";
		system("pause");
		system("cls");
		cout << "You are thinking about every moment of you life\nPlease  wait a moment as you think carefully what you plan on doing after you leave this building\n \n";
		Beep(400, 150); // beep is the sound beeping first number is the tone hi or low and second number is how long the tone goes
		Beep(300, 150);
		Beep(200, 150);
		
		system("pause");
		system("cls");
		chapter1();
	}
	else
	{
		cout << "You are doing it wrong! Press either '1' or '2', or '3' nothing else!\n";
	}	
}
void quest()//quest for the user to input options and end of first chapter
{
	system("cls");
	cout << "You reach a valley with giant gate with the guard guarding the Gate.\n\n";
	cout << "What would you like to do?\n";
	cout << "*************************************************************************************************************\n";
	cout << "1: Get the Directions to find another route through the valley\n";
	cout << "2: Pay him to Let you through these gates through the valley to find your own path to uncover mystries\n";
	cout << "*************************************************************************************************************\n";

	cout << "\nEnter your choice: ";
	cout << "\n>\t";

	int path = 0;
	while (!(cin >> path) || path < 1 || path > 3)
	{
		cout << "invalid variable. must be in range of asked choice.\n";
		cin.clear();
		cin.ignore(100, '\n');
	}
	if (path == 1)
	{
		system("cls");
		cout << "\nYou: What way to the next city?\n";
		cout << "Guard: Just follow the valley to the left. But beware of monsters\n\n";
		cout << "you go and follow the valley and end up being attacked by several monsters and decid to fight back but you are not strong enough\n";
		cout << "wounded and unconcious and the guard petrolling in the area find you and send you back to the hospital\n";
		cout << "You end up back in hospital";
		chapter1();
	}
	else if (path == 2)
	{

		system("cls");
		cout << "\nYou:\t I am going to find a way out!\n";
		cout << "Guard:\t You are insane. You will get killed out there.\n";
		cout << "You:\t I have my secrets and I know my way out. I want to know the truth and i will do whatever it takes to find out about this world.\n\n";
		cout << ">> You Pay the guard some gold";
		cout << ">> The guard is distracted by the gold and starts counting them\n\n";
		cout << ">> Meanwhile somewhere faraway\n ";
		cout << ">> Big explosion happens and guards go take a look leaving the gate opened\n";
		cout << ">> You without thinking go through the gate across the valley\n\n";
		cout << ">> You are amazed as you find a new city with rich people and rich atmosphere.\n";
		cout << ">> A City you never imagined to ever see in a dream ";
		cout << ">> You entered the city and have ended your first chapter and are going into a whole new world\n";

		system("pause");
		system("cls");
		cout << "TO BE CONTINUED\n\n\n" << endl;
		system("color e");
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "******************                End Of                   *********************\n";
		cout << "******************             Chapter ONE                 *********************\n";
		cout << "******************           The Eternal Dream to Hell     *********************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		cout << "********************************************************************************\n";
		Beep(523, 500);
		Beep(587, 500);
		Beep(659, 500);

	}
}
void gameOver()//gamer over menu
	{
	system("color e");
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "******************              Game Over                  *********************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	cout << "********************************************************************************\n";
	Beep(300, 500);
	Beep(100, 500);
	system("pause");
	system("cls");
	Game();
	}